package logg;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logg {

    File loggFile;

    public Logg(String logUrl){
        loggFile = new File(logUrl + "\\logg.txt");
        newSession();
    }

    // return the current time
    private String currentTime(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }
    // Loggs action
    public void loggAction(String action){
        writeToFile(action, true);
    }
    // loggs error
    public void loggError(String errorMessage){
        writeToFile(errorMessage, false);

    }

    // Makes the logg easy to read - enter "new" and "end" entry
    private void newSession(){
        writeToFile("\n\nNEW ENTRY", false);
    }
    public void endSession(){
        writeToFile("END ENTRY", true);
    }
    // "stpos" the timer
    public void endTimer(long startTime){
        long time = (System.nanoTime() - startTime)/1000000; // TO GET MILLISECONDS

        writeToFile("Finished in: " + time + " ms." , false);

    }


    // Writes "toBeLogged" in logg.txt
    private void writeToFile(String toBeLogged, boolean withTime) {

        String newFeed = "";
        if(withTime){
            newFeed = currentTime();
        }
        newFeed += " - " + toBeLogged + "\n";

        //Write into the logg
        try {
            //Create a FileWriter object to write in the file
            FileWriter fWriter  = new FileWriter(loggFile, true);

            //Write into the file
            fWriter.write(newFeed);

            //Close the file writer object
            fWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

