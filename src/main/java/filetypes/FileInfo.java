package filetypes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileInfo {
    File file;
    String fileType;

    public FileInfo(String filename){
        file = new File(filename);
        fileType = filename.split("\\.")[1];
    }

    public long getFileSize(){
        try{
            return Files.size(Path.of(file.getAbsolutePath()));
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public String getAllInfo(){
        return "File type: " + fileType +  "\nFile size: " + getFileSize() + " bytes";
    }


}
