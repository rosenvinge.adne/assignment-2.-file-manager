package filetypes;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class TextFileName extends FileInfo{

    public TextFileName(String filename) {
        super(filename);
    }


    // also needs to include the number of lines
    @Override
    public String getAllInfo(){
        return "File type: " + fileType +  "\nFile size: " + getFileSize() + " bytes" + "\n Lines: " + countLines();
    }

    //Counts the number of lines in the file
    public int countLines(){
        int lineCount = 0;
        try{
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                lineCount += 1;
                scanner.nextLine();
            }
            scanner.close();
        }catch (FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        return lineCount;
    }

    //returns boolean -> file contains phrase
    public boolean fileContainString(String string){
        try{
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                final String lineFromFile = scanner.nextLine().toLowerCase();
                if(lineFromFile.contains(string)) {
                    scanner.close();
                    return true;
                }
            }
            scanner.close();
        }catch (FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        return false;
    }

    //Counts the number of times 'target' occurs
    public int countInstants(String target){
        int count = 0;
        target = target.toLowerCase();

        try{
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()){
                String line = scanner.nextLine();

                String[] temp = line.split(" ");

                for(String word: temp){
                    int lastIndex = 0;

                    while(lastIndex != -1){
                        word = word.toLowerCase();

                        lastIndex = word.indexOf(target,lastIndex);

                        if(lastIndex != -1){
                            count ++;
                            lastIndex += target.length();
                        }
                    }
                }
            }
        }catch (FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }
        return count;
    }
}
