import filetypes.FileInfo;
import filetypes.TextFileName;
import logg.Logg;
import menu.Menu;

import java.io.File;

public class FileManager {

    public static Logg logg;
    private static File resourcesFile;
    private static boolean running = true;
    private static Menu menu;
    private static long operationStartTime;

    //COMMAND: view 'type', prints all available files in the right type
    private static void viewFiletypes(String fileType){

        boolean result = false;
        String[] allFileNames = findFilesInResources();

        for (String fileName:
             allFileNames) {
            if (fileName.split("\\.")[1].equals(fileType)) {
                print(fileName);
                result = true;
            }
        }
        if (result) print("Search done.");
        else print("No results");

    }

    //COMMAND: view, prints all results
    private static void viewFolder(){
        String[] allFileNames = findFilesInResources();

        if (allFileNames != null) {
            print("Total elements :" + allFileNames.length);
            for (String content : allFileNames) {
                System.out.println("    - " + content);
            }
        }else{
            System.out.println("--folder empty--");
        }
    }

    // CHECK: if file exists in resource folder
    private static boolean fileExists(String targetFileName){
        String[] allFileNames = findFilesInResources();
        for (String name:allFileNames) {
            if (targetFileName.equals(name)) return true;
        }
        return false;
    }

    // Returns the relevant files in resources
    private static String[] findFilesInResources() {
        try{
            File folderPath = new File(resourcesFile.getPath());
            //  Makes sure we only see files we want to see.
            return folderPath.list((dir, name) -> name.contains(".") && !name.contains(".class"));
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        return new String[0];
    }

    //CHECK: checks if the filename is written correctly - if not adds the filetype
    private static String completeFileName(String fileName){
        String[] allFiles = findFilesInResources();

        for (String f:allFiles) {

            if (f.split("\\.")[0].equalsIgnoreCase(fileName.split("\\.")[0])){
                return f;
            }
        }
        return fileName;
    }

    //COMMAND: info, prints info about all files.
    private static void info(){
        String[] allFiles = findFilesInResources();
        for (String file: allFiles) {
            infoFile(file);
        }
    }
    //COMMAND: info 'filename', prints info connected to chosen file.
    private static void infoFile(String fileName){

        fileName = completeFileName(fileName);
        FileInfo file;

        if(!fileExists(fileName)){ // CHECK: if file exists
            printError(fileName + " File not found");
        }else{

            if (isTxt(fileName)){
                file = new TextFileName(resourcesFile.getAbsolutePath() + "\\" + fileName);
            }else{
                file = new FileInfo(resourcesFile.getAbsolutePath() + "\\" + fileName);
            }

            print( " \n\n" + fileName + ":\n" + file.getAllInfo());

        }


    }

    // Handles searching though a txt file.
    // allSearchInfo format: "filename.txt-searchTerm-y/n" (y/n is count or not)
    private static void search(String allSearchInfo){
        noteTime();

        String[] cmd = allSearchInfo.split("-");
        /* cmd[0] - filename, cmd[1] - search term, cmd[2] - y==count every occurrence n== return boolean*/

        // Added to make the code easy to read
        String fileName = completeFileName(cmd[0]);
        String term = cmd[1];
        String countInstance = cmd[2];

        if (!fileExists(fileName)){
            printError("\""+ fileName+"\" File not found");
        }else if (!isTxt(fileName)){
            printError(fileName + " can not be read");
        }
        else {
            TextFileName tx = new TextFileName(resourcesFile.getAbsolutePath() + "\\" + createTextFile(fileName));
            if (countInstance.equals("y")){
                int res = tx.countInstants(term);
                print("Search term: '" + term + "' in "+ fileName + ", Results:" + res);
            }else{
                if (tx.fileContainString(term)){
                    print(fileName + " contains the term '"+ term + "'.");
                }else{
                    print(fileName + " does not contains the term '"+ term + "'.");
                }
            }
        }
    }

    // CHECK: if file is .txt
    private static boolean isTxt(String fileName){
        return fileName.split("\\.")[1].equals("txt");
    }

    // Handles the 4 main commands and cals the right methods, part of the main loop
    private static void runCommand(String cmd){

        String[] split = cmd.split(" ");

        print("RUNNING COMMAND:" + cmd);

        switch (split[0]) {
            case "exit":
                running = false;
                logg.endSession();
                break;
            case "view":
                noteTime();
                if (split.length == 1) {
                    viewFolder(); // View all relevant files
                } else {
                    viewFiletypes(split[1]); // Search by filetype
                }
                break;
            case "info":
                noteTime();
                if (split.length == 1) {
                    info();
                }else{
                    infoFile(split[1]);
                }
                break;
            case "search":
                if(split.length == 1){
                    search(menu.searchInputNoName()); // NEEDS TO SPECIFY WITCH FILE
                }else{
                    search(menu.searchInput(split[1]));
                }
                break;
            default:
                printError(split[0] + " is not a legal command");

        }
        if (running) printTime(operationStartTime);
    }


    // Boots the program - also contains the main loop
    public static void main(String [] arg){
        String currentPath = System.getProperty("user.dir");

        //Locates the resources file
        File f = new File(currentPath).getAbsoluteFile();
        resourcesFile = new File(f.getPath()+"\\target\\classes");

        logg = new Logg(resourcesFile.getAbsolutePath()+"\\logg");

        menu = new Menu();

        while (running){
            runCommand(menu.mainMenu());
        }
    }
    //TODO COMMENT
    private static String createTextFile(String fileName){
        String[] allFiles = findFilesInResources();
        fileName = completeFileName(fileName);

        for (String file:allFiles) {
            if (fileName.equals(file.toLowerCase())) return file;
        }
        return fileName;
    }

    // Starting time for all the commands
    private static void noteTime(){
        operationStartTime = System.nanoTime();
    }

    // Prints to the log:
    private static void printError(String e){
        System.out.println("Error: " + e);
        logg.loggError("Error: " + e);
    }
    private static void print(String s){
        System.out.println("FileManager: " + s);
        logg.loggAction(s);
    }
    private static void printTime(long t){
        logg.endTimer(t);
    }
}
