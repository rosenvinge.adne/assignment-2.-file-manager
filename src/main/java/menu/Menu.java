package menu;

import java.util.Objects;
import java.util.Scanner;

/*
* Class description:
*
*   Handles the man menu and all inputs from the user
*   Also contains all the formating 
* */

public class Menu {

    // Variables that lets me edit the font
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String WHITE_BOLD_BRIGHT = "\033[1;97m"; // WHITE

    private static final String[] OPTIONS_FORMAT = {
            "["+ANSI_BLUE+"exit"+ANSI_RESET+"]\n",
            "["+ANSI_BLUE+"view"+ANSI_RESET+"]",
            "["+ANSI_BLUE+ "view " + ANSI_RESET+"'filetype']\n",
            "["+ANSI_BLUE+ "info " + ANSI_RESET+"'FileName']\n",
            "["+ANSI_BLUE+ "search " + ANSI_RESET+ "FileName]\n",
    };

    private static final String[] OPTIONS = {
            "exit",
            "view",
            "info",
            "search"
    };

    public Menu(){ }

    // Handles all inputs from the user
    private String userInput(){

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        //scanner.close();
        return input.toLowerCase();
    }

    //Displays "menu" and the options
    private void displayOptions(){
        System.out.println(WHITE_BOLD_BRIGHT + "\n\n    --    MAIN    --    " + ANSI_RESET);
        System.out.println("Pick one og the following commands");
        System.out.println("   --    OPTIONS   --    ");
        for (String o:
             OPTIONS_FORMAT) {
            System.out.print(o);
        }
    }

    // If the program needs more information  - message print in bold
    public String getInput(String m){
        System.out.print(WHITE_BOLD_BRIGHT + m + ANSI_RESET);
        return userInput();
    }

    // Displays options - return command
    public String mainMenu(){

        String command;
        displayOptions();
        command = userInput();

        return command;
    }

    //  gets search term and toggles count/exists  
    public String searchInput(String fileName){
        String term = getInput("Enter search term:");
        String times = getInput("Do you wish to know how many times '" + term + "' ocurres? [y][n]:");
        return fileName + "-" + term + "-" + times;
    }
    public String searchInputNoName(){
        return searchInput(getInput("Enter file name"));
    }





}
