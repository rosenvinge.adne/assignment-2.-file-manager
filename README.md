# Assignemt 2 - File manager

Everything shuld be in order, ony thing I haven't implemented is two seperate algorythyms to search through a txt file.

# Program Files:

Location: /src/main/java.

Main file: FileManager.java

Packages: logg, menu and filetype.

# menu

Handles all inputs from the user and displays all options

# logg

prints to the logg and displays all messages and errors

# filetype

FileInfo - find the size and filetype of a file.

TextFileName, Extends FileInfo - algorythyms for locating a word in a file, counting a phrase in a file and counting lines.


# Functionality:

From Main menu the user have the following options:
'exit' - exits the system
'view' - view all file names
'view <filetypes>' - view all file names of the type
'info' - displays file information for all the files
'info <filename>' - case insencetive and works with no filetype - displays file information for single file.
info displays file size, filetype, and lines count if relevant
'search <filename>' - system asks for search term and if the user wants the term count or not, then displays the information

